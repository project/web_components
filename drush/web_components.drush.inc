<?php
/**
 * @file
 * Drush integration for Web Components API.
 */

/**
 * Implements hook_drush_command().
 */
function web_components_drush_command() {
  $items = array();

  $items['webcomponents-download-webcomponentsjs'] = array(
    'callback'    => '_web_components_drush_download_webcomponentsjs',
    'description' => dt('Downloads the webcomponents.js'),
    'bootstrap'   => DRUSH_BOOTSTRAP_DRUSH,
    'aliases'     => array('wdlw'),
    'arguments'   => array(
      'path' => dt('Optional, not recommended. A destination for the Web Components polyfill.'),
    ),
  );

  return $items;
}

/**
 * Helper function to download web components polyfill.
 */
function _web_components_drush_download_webcomponentsjs() {
  $args = func_get_args();
  if ($args[0]) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }
  $library = libraries_detect($args[0]);
  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Build full path.
  $filename = $library['webcomponentsjs']['machine name'];
  $drush_context = drush_get_context('DRUSH_DRUPAL_ROOT');

  // Build full path.
  $library_path = 'sites/all/libraries/' . $library['webcomponentsjs']['machine name'];
  $path = $drush_context . '/' . $library_path;

  // Download the zip archive
  _drush_download_file($library['webcomponentsjs']['download url'], $path, TRUE);

  // If archive was downloaded, unzip it and delete.
  if (is_file($filename)) {
    // Open the Zip file
    $zip = new ZipArchive;
    $extractPath = $drush_context . '/sites/all/libraries/';
    if($zip->open($path) != "true"){
      echo "Error :- Unable to open the Zip File";
    }
    // Extract Zip File
    $zip->extractTo($extractPath);
    $zip->close();
    // Remove the zip archive
    drush_op('unlink', $filename);
    // Rename library directory
    $dir = scandir($extractPath);
    foreach($dir as $val) {
      $dirname = stripos($val, 'webcomponentsjs-');
      if($dirname !== false) {
        $target = $extractPath . $val;
        $newName = $extractPath . 'webcomponentsjs';
        rename($target, $newName);
      }
    }
    // Log success.
    drush_log(dt('The @name library has been downloaded to @path', array(
      '@name' => $library['webcomponentsjs']['module'],
      '@path' => $library_path,
    )), 'success');
  }
  else {
    // Log the error.
    drush_log(dt('Drush was unable to download @name to @path', array(
      '@name' => $library['webcomponentsjs']['module'],
      '@path' => $library_path,
    )), 'error');
  }
}
