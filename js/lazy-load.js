/**
 * @file
 * Lazy loading of html imports.
 */

(function(Drupal, document, undefined) {
  Drupal.behaviors.webComponentsLazyLoad = {
    attach: function(context, settings) {
      // Get web components to load.
      var elements = Drupal.settings.webComponents.elementsPath;

      // Create html import for each web compoent.
      elements.forEach(function(elementURL) {
        var elImport = document.createElement('link');
        elImport.rel = 'import';
        elImport.href = elementURL;
        document.head.appendChild(elImport);
      });
    }
  };
})(Drupal, document, undefined);
