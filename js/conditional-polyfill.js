/**
 * @file
 * Conditionally loads webcomponents polyfill if needed.
 */

(function(Drupal, document, undefined) {
  Drupal.behaviors.webComponentsConditional = {
    attach: function(context, settings) {
      // Check if browser support web components.
      var webComponentsSupported = ('registerElement' in document
        && 'import' in document.createElement('link')
        && 'content' in document.createElement('template'));

      // If web components not supported load polyfill.
      if (!webComponentsSupported) {
        var wcPoly = document.createElement('script');
        wcPoly.src = '/' + Drupal.settings.webComponents.polyfillPath;
        document.head.appendChild(wcPoly);
      }
    }
  };
})(Drupal, document, undefined);
