<?php
/**
 * @file
 * Administrative functionality for Web Components API.
 */

/**
 * Implements hook_admin().
 *
 * Web Components API administration settings.
 */
function web_components_admin() {
  $error_message = '';
  $polyfill_library = libraries_detect('webcomponentsjs');

  if ($polyfill_library && $polyfill_library['installed']) {
    $polyfill = TRUE;
  }
  else {
    $polyfill = FALSE;
    $error_message = '<div class="messages error">' . $polyfill_library['error message'] . '</div>';
    variable_set('web_components_polyfill_enable', FALSE);
    variable_set('web_components_polyfill_conditional', FALSE);
  }

  $form = array();

  $form['web_components'] = array(
    '#type' => 'fieldset',
    '#title' => t('Web Components settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
  );

  $form['web_components']['web_components_lazy_load'] = array(
    '#type' => 'checkbox',
    '#title' => t('Lazy load'),
    '#description' => t('Lazy load web components on page load.'),
    '#default_value' => variable_get('web_components_lazy_load', TRUE),
  );

  $form['webcomponentsjs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Web Components polyfill'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => FALSE,
    '#prefix' => $error_message,
  );

  $form['webcomponentsjs']['web_components_polyfill_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add Web Components polyfill'),
    '#description' => t('Adds webcomponentsjs polyfill for browsers without full Web Components support.'),
    '#default_value' => variable_get('web_components_polyfill_enable', TRUE),
    '#disabled' => !$polyfill,
  );

  $webcomponentsjs_settings_states = array(
    'visible' => array(
      ':input[name="web_components_polyfill_enable"]' => array('checked' => TRUE),
    ),
  );

  $form['webcomponentsjs']['web_components_polyfill_conditional'] = array(
    '#type' => 'checkbox',
    '#title' => t('Polyfill conditional load'),
    '#description' => t('Adds webcomponentsjs polyfill conditionally depending on current browser web components support.'),
    '#default_value' => variable_get('web_components_polyfill_conditional', TRUE),
    '#states' => $webcomponentsjs_settings_states,
  );

  $form['webcomponentsjs']['web_components_polyfill_compression_type'] = array(
    '#type' => 'radios',
    '#title' => t('Web Components polyfill compression level'),
    '#options' => array(
      'min' => t('Production (minified)'),
      'none' => t('Development (uncompressed)'),
    ),
    '#states' => $webcomponentsjs_settings_states,
    '#default_value' => variable_get('web_components_polyfill_compression_type', 'min'),
  );

  $form['webcomponentsjs']['web_components_polyfill_features_variant'] = array(
    '#type' => 'radios',
    '#title' => t('Web Components polyfill features support'),
    '#description' => t('For more information visit !webcomponents.', array('!webcomponents' => l(t('Web Components web-site'), 'http://webcomponents.org/'))),
    '#options' => array(
      'standard' => t('Standard'),
      'lite' => t('Lite'),
    ),
    '#states' => $webcomponentsjs_settings_states,
    '#default_value' => variable_get('web_components_polyfill_features_variant', 'standard'),
  );

  return system_settings_form($form);
}
