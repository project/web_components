<?php
/**
 * @file
 * Documentation for Web Components API.
 */

/**
 * Inform the web_components module about new web components.
 *
 * @return array
 *   An array of web components to be controlled by Web Components API, keyed by
 *   web component name. Each web component record should be an array with the
 *   following key/value pairs:
 *   - name: The human-readable name of the web component.
 *   - description: The web component description.
 *   - path: The path to the web component library.
 *   - autoload: Enable web compolent load on every page.
 *   - weight: The weight of the web component.
 *
 * @see web_components_get_web_component_info()
 */
function hook_web_component_info() {
  // Define example web component.
  $components['example_web_component'] = array(
    'name' => t('Example Web Component'),
    'description' => t('Example custom web component'),
    'path' => libraries_get_path('example_web_component'),
    'autoload' => TRUE,
    'weight' => 1,
  );

  return $components;
}

/**
 * Alter the list of web components.
 *
 * @param array $components
 *   An array of web components to be controlled by Web Components API,
 *   keyed by web component name.
 *
 * @see hook_web_component_info()
 * @see web_components_get_web_component_info()
 */
function hook_web_component_info_alter(array &$components) {
  // Remove the example web component.
  unset($components['example_web_component']);
}
